# Contribution Guidelines

I (Thinker Nanashi), would love you to help us (https://gitlab.com/thebiteof87/) improve this project. To help us keep this repo stable, I request that contributions adhere to the following guidelines.

-    Explain why you’re making a change. Even if it seems self-evident, please take a sentence or two to explain why your change or addition should happen. It’s especially helpful to articulate how this change alters or improves functionality in a beneficial manner.

-    Be respectful to other users' contributions. Don't try to stealthily usurp someone else's contrib, ad don't try to shame someone in discussion for a proposed contrib. Both of these actions are considered unhealthy in regard to project networking/outreach, as it discourages others from further interacting with the project.

-   Don't propose proprietary/stolen code. If implementing code from another public repository, please provide the necessary licenses for the repo in question. I don't ewant to get sued, y'know.

In general, the more you can do to help me understand the change you’re making, the more likely I’ll be to accept your contribution quickly.
